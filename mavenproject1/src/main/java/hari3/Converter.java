package hari3;

import java.util.Scanner;

class Utility{
    static void tampilkanMenu(){
        System.out.println(" ==== Menu ====");
        System.out.println("1. Celcius to Fahrenheit");
        System.out.println("2. Dollar to Rupiah");
        System.out.println("0. Exit");
    }
}

public class Converter {
    public static void main(String[] args) {
        while (true) {
            //menampilkan menu
            Utility.tampilkanMenu();
          
            //membaca pilihan user
            Scanner scan = new Scanner(System.in);
            System.out.println("Pilihan anda:");
            int pilihan;
            try {
                pilihan = scan.nextInt();          
            } catch (Exception e) {
                System.out.println("Pilihan anda salah (pilih 0 - 2)");
                continue;
            }

            if (pilihan == 0) {
                break;
            }else if(pilihan > 2){
                continue;
            }
            CelciusToFahrenheit cf = new CelciusToFahrenheit();
            Convertible k = cf;
            Convertible[] konverters = new Convertible[5];
            konverters[1] = new CelciusToFahrenheit();
            konverters[2] = new DollarToRupiah();

            Convertible konverterPilihan = konverters[pilihan];
            float nilai;
            while(true){
                scan = new Scanner(System.in);
                System.out.println("Masukkan nilai " + konverterPilihan.satuanAsal() + ":");
                try{
                    nilai = scan.nextFloat();
                    break;
                }catch(Exception e){
                    System.out.println("Masukkan angka!");
                }
            }
            
            konverterPilihan.setNilai(nilai);
            System.out.println(nilai + " " + konverterPilihan.satuanAsal() + " setara "
                    + konverterPilihan.convert() + " " + konverterPilihan.satuanTujuan());
        }
    }
}
