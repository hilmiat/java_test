
package hari3;

public class CelciusToFahrenheit implements Convertible{
    float suhu;
    @Override
    public String satuanAsal() {
        return "Celcius";
    }

    @Override
    public String satuanTujuan() {
        return "Fahrenheit";
    }

    @Override
    public void setNilai(float nilai) {
        this.suhu = nilai;
    }

    @Override
    public String convert() {
        return ""+9f/5 * this.suhu + 32;
    }
    
}
