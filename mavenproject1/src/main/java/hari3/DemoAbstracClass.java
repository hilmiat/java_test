package hari3;
abstract class Animal{
    public abstract void animalSound();
    public void sleep(){
        System.out.println("ZZzzzz...");
    }

}

class Pig extends Animal{
    @Override
    public void animalSound() {
        System.out.println("Oink..Oink...");
    }
}

class Dog extends Animal{
    public void animalSound(){
        System.out.println("Bark..Bark..");
    }
}
public class DemoAbstracClass {
    public static void main(String[] args) {
        //polymorphisme
        Animal a = new Pig();
        a.animalSound();
        a.sleep();
        
        a = new Dog();
        a.animalSound();
        a.sleep();
        
       
    }
}
