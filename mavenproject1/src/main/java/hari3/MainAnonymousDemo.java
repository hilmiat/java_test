package hari3;
class Polygon{
    public void display(){
        System.out.println("Inside polygon class");
    }
}
class AnonymousDemo{
    public void demo(){
        //buat class baru, subclass dari polygon
        Polygon p = new Polygon(){
            int baru = 5;
            
            @Override
            public void display() {
                System.out.println("Inside inner class");
            }       
        };
        p.display();
    }
}
public class MainAnonymousDemo {
    public static void main(String[] args) {
        AnonymousDemo ad = new AnonymousDemo();
        ad.demo();
    }
}
