package hari3;
public interface Convertible {
    public String satuanAsal();
    public String satuanTujuan();
    public void setNilai(float nilai);
    public String convert();

}
