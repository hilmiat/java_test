package hari3;
class OuterClass{
    int x = 10;
    void doSomething(){
    
    }
    //membuat inner class
    class InnerClass{
        int y = 5;
        public int myInnerMethod(){
            return x;
        }
        
    }
}

public class DemoInnerClass {
    public static void main(String[] args) {
        OuterClass myOuter = new OuterClass();
        OuterClass.InnerClass myInner = myOuter.new InnerClass();
        System.out.println(myInner.myInnerMethod() + myInner.y);
    }
}
