
package hari3;

public class DollarToRupiah implements Convertible{
    float uang;
    @Override
    public String satuanAsal() {
        return "Dollar";
    }

    @Override
    public String satuanTujuan() {
        return "Rupiah";
    }

    @Override
    public void setNilai(float nilai) {
        this.uang = nilai;
    }

    @Override
    public String convert() {
        return ""+this.uang * 13000;
    }
    
}
