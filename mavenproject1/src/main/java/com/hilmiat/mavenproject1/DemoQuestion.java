package com.hilmiat.mavenproject1;

import java.util.Scanner;

public class DemoQuestion {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Question q = new Question();
        q.setText("Siapa penemu java?");
        q.setAnswer("James Gosling");
        q.display();
        
        System.out.println("Jawaban anda:");
        String response = in.nextLine();
        System.out.println(q.checkAnswer(response));
        
        ChoiceQuestion cq = new ChoiceQuestion();
        cq.setText("Siapa penemu java?");
        cq.addChoice("James Bond", false);        
        cq.addChoice("James Gosling", true);

    }
}
