package com.hilmiat.mavenproject1;
public class Question {
    private String text;
    private String answer;

    public Question() {
        text="";
        answer="";
    }
    /**
     * Set pertanyaan
     * @param text String pertanyaan
     */
    public void setText(String text) {
        this.text = text;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
    
    public boolean checkAnswer(String response){
        return response.equals(answer);
    }
    
    public void display(){
        System.out.println(text);
    }
    
}
