package com.hilmiat.mavenproject1;

import java.util.ArrayList;

public class ChoiceQuestion extends Question{
    private ArrayList<String> choices;

    public ChoiceQuestion() {
        choices = new ArrayList<String>();
    }
    
    public void addChoice(String choice,boolean correct){
        choices.add(choice);
        if(correct){
            String choiceString = "" + choices.size();
            setAnswer(choiceString);
        }
    }

    @Override
    public void display() {
        for(int i =0;i< choices.size(); i++){
            int  choicenumber = i+1;
            System.out.println(choicenumber+":"+choices.get(i));
        }
    }
    
    
    
}
