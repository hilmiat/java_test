package day3;

public class DemoInnerClass {
    public static void main(String[] args) {
        OuterClass myOuter = new OuterClass();
        OuterClass.InnerClass myInner = myOuter.new InnerClass();
        System.out.println(myInner.y + myOuter.x);
        //memanggil innerMethod
        System.out.println(myInner.myInnerMethod());
    }
}
