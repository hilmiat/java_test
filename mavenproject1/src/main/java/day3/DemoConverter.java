package day3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DemoConverter {

    public static void main(String[] args) {

        int pilihan;
        Scanner scan;
        while (true) {
            System.out.println("==== Menu ====");
            System.out.println("1. C to F");
            //tambahkan menu lainnya...
            System.out.println("0. Exit");
            scan = new Scanner(System.in);
            System.out.println("pilih 1-4:");
            try {
                pilihan = scan.nextInt();
                System.out.println("pil:" + pilihan);
            } catch (InputMismatchException eee) {
                System.out.println("Pilihan anda tidak tepat");
                continue;
            }
            if (pilihan == 0) {
                break;
            }
            Convertible[] konverter = new Convertible[5];
            konverter[1] = new CelciusToFahrenheit();
            //tambahkan pilihan konversi lainnya
            try {
                System.out.println("Masukkan nilai " + konverter[pilihan].satuanAsal());
            } catch (Exception e) {
                System.out.println("Pilih 1-4:");
                continue;
            }
            float nilai;
            while (true) {
                scan = new Scanner(System.in);
                try {
                    nilai = scan.nextFloat();
                    break;
                } catch (Exception e) {
                    System.out.println("Masukkan nilai dengan benar...");
                }
            }
            konverter[pilihan].setNilai(nilai);
            System.out.println("Nilai " + nilai + " " + konverter[pilihan].satuanAsal() + " = "
                    + konverter[pilihan].convert() + " " + konverter[pilihan].satuanTujuan());

        }
    }
}
