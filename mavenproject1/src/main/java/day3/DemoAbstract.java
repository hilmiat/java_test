
package day3;
//abstract class
abstract class Animal{
    //method abstract (tidak memiliki body)
    public abstract void animalSound();
    //regular method
    public void sleep(){
        System.out.println("ZZzz");
    }
}

//class Pig merupakan turunan dari animal
class Pig extends Animal{

    @Override
    public void animalSound() {
        //isi dari method abstract animalSound didefinisikan disini
        System.out.println("Oink..Oink..");
    }
    
}
public class DemoAbstract {
    public static void main(String[] args) {
        Pig myPig = new Pig();
        myPig.animalSound();
        myPig.sleep();
    }
}
