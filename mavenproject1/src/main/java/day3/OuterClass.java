package day3;
public class OuterClass {
    int x = 10;
    class InnerClass{
        int y = 5;
        public int myInnerMethod(){
            //mengakses x yang merupakan property OuterClass
            return x;
        }
    }
}
