package day3;

class Polygon {
    public void display(){
        System.out.println("Inside the Polygon class");
    }
}
class AnonymousDemo{
    public void createClass(){
        //Membuat class anonim yang merupakan turunan class Polygon
        Polygon p1 = new Polygon(){
            @Override
            public void display() {
                System.out.println("Inside an anonymous class");
            } 
        };
        p1.display();
    }
}
public class MainAnonymousDemo {
    public static void main(String[] args) {
        AnonymousDemo an = new AnonymousDemo();
        an.createClass();
    }
}
